
// npm init -y
// npm install express
//touch .gitignore
//npm install nodemon
// npm start
// require directive tells us to load the express module

const express = require('express')

const app = express();

const port = 4000;

//middlewares

app.use(express.json())
		// allows app to read a json data
app.use(express.urlencoded({extended: true}));
		// allows app to read a json data
		//  by default, infomarion received from the url can only be received as string or array
		//with extended : true, this allows to receive information in other data types such as object

//  mock database

let users = [
	{
		email: "nezukoKamado@gmail.com", 
		username: "nezuko01",
		password: "letMeOut",
		isAdmin: false
	},
	{
		email: "TanjiroKamado@gmail.com", 
		username: "gonpachiro",
		password: "IamTanjiro",
		isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com", 
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: true
	}
]

let loggedUser;

app.get('/',(req,res) =>{
	res.send('Hello World')
})

//GET method

app.get('/hello',(req,res) =>{
	res.send('Hello from batch 131')
})

//POST Method

app.post('/',(req,res)=>{
	console.log(req.body);
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age}. I could be described as ${req.body.description}`)
})

app.post('/users/register',(req,res)=>{
	console.log(req.body);
	let newUser = {
		email : req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	}
	users.push(newUser)
	console.log(users)

	res.send(`User ${req.body.username} has successfully registered.`)
})

app.post('/users/login',(req,res)=>{
	console.log(req.body);

	let foundUser = users.find((user) =>{
		return user.username === req.body.username && user.password === req.body.password
	})
	if(foundUser !== undefined){
		let foundUserIndex = users.findIndex((user) =>{
			return user.username === foundUser.username
		})
		loggedUser = foundUser;
		console.log(loggedUser)
		res.send('Thank you for logging in.')
	}
	else {
		loggedUser = foundUser;
		res.send('Login Failed, Wrong credential')
	}
})

app.put('/users/change-password',(req,res) => {
	let message;
	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been change.`
			break;
		}
		else{
			message = `User not found.`
		}
	}
	res.send(message)
})
// app - server
// get - HTTP method
// '/' - route name or endpoint
// (req,res) - request and response - will handle the request and the response
// res.send - combines writeHead() and end(), used to send response to our client

app.get('/home',(req,res) =>{
	res.send('Welcome to the world')
})

app.get('/users',(req,res) =>{
	res.send(users)
})
app.put('/users/delete-user',(req,res) => {
	let message;
	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username && req.body.password === users[i].password ){
			

			message = `User ${req.body.username}'s password has been deleted.`
			users.splice(i,1)
			break;
		}
		else{
			message = `User not found.`
		}
	}
	res.send(message)
})



		// listen to the port and returning message in the terminal.
app.listen(port, () => console.log(`The Server is running at port ${port}`))